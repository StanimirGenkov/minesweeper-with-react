import React, { useState } from 'react';

export const ModeProvider = ({ children }) => {
    // const [developMode, toggleMode] = useState(false);
    // const [ElenaMode, toggleElenaMode] = useState(false);
    const [areFieldsVisible, toggleFieldsVisible] = useState(false);

    // const setMode = () => {
    //     toggleMode(developMode? false : true);
    // };

    // const setElenaMode = () => {
    //     toggleElenaMode(ElenaMode? false : true);
    // };

    const setAllInvisible = () => {
        toggleFieldsVisible(false);
        // console.log(areFieldsVisible);
    };

    const showAll = (state) => {
        if(state==='sign') {
            toggleFieldsVisible(false);
            return;
        };

        if (areFieldsVisible) {
            toggleFieldsVisible(false);
        } else {
            toggleFieldsVisible(true);
        };
    };

    return (
        <ModeContext.Provider value={{areFieldsVisible, setAllInvisible, toggleFieldsVisible, showAll}}>
            {children}
        </ModeContext.Provider>
    );

};

export const ModeContext = React.createContext({
    developMode: "false",
    toggleMode: () => { },
    ElenaMode: "false",
    toggleElenaMode: () => { }
});