import React from 'react';
import './App.css';
import Header from './Base/Header/Header';
import Minefield from './Base/Minefield/Minefield';

function App() {
  return (
    <div className="App">
        <Header />
        <Minefield />
        {/* <TestComponent /> */}
    </div>
  );
}

export default App;
