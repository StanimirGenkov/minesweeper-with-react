import React, { useContext, useEffect, useState } from 'react';
import { ModeContext } from '../../providers/ModeProvider';
import Mine from '../Mine/Mine';
import './Minefield.css';

const Minefield = (props) => {
    const [mineField, generateField] = useState([]);
    const rows = 5;
    const cols = 5;
    const mines = 1;
    const { areFieldsVisible, setAllInvisible } = useContext(ModeContext);
    const [matrix, setMatrix] = useState([]);
    const [minesLeft, setMinesLeft] = useState(mines);
    const [newGame, setNewGame] = useState(0);

    const startNewGame = () => {
        setNewGame(newGame + 1);
        setAllInvisible(false);
        generateField([]);
        generateField(matrixCreate());
        setMinesLeft(mines);
    };

    const isInRange = (row, col) => {
        if ((row < 0) || (col < 0) || (row >= rows) || (col >= cols)) {
            return false;
        } else {
            return true;
        };
    };

    const calculateValueOfField = (row, col, matrix) => {
        let result = 0;

        for (let i = -1; i <= 1; ++i) {
            for (let j = -1; j <= 1; ++j) {
                if (isInRange((row + i), (col + j))) {
                    if (i === 0 && j === 0) {
                        continue;
                    } else {
                        if (matrix[row + i][col + j].isMine === true) {
                            ++result;
                        };
                    };
                };
            };
        };

        return result;
    };

    const matrixCreate = () => {
        let newMatrix = [];
        for (let i = 0; i < rows; ++i) {
            let row = []
            for (let j = 0; j < cols; ++j) {
                const cell = {
                    // isFlagged: false,
                    // isUnsure: false,
                    isMine: false,
                    position: [i, j],
                    value: 0,
                    isOpened: false,
                    isMarked: false,
                };
                row.push(cell);
            };
            newMatrix.push(row);
        };

        for (let minesSetted = 0; minesSetted < mines;) {
            const row = Math.floor(Math.random() * rows);
            const col = Math.floor(Math.random() * cols);
            if (newMatrix[row][col].isMine === false) {
                newMatrix[row][col].isMine = true;
                ++minesSetted;
            } else {
                continue;
            };
        };

        for (let i = 0; i < rows; ++i) {
            for (let j = 0; j < cols; ++j) {
                newMatrix[i][j].value = calculateValueOfField(i, j, newMatrix);
            };
        };
        generateField(newMatrix);

        return newMatrix;
    };

    useEffect(() => {
        startNewGame();
    }, []);

    const revealEmptiness = (row, col) => {

        if (mineField[row][col].isOpened === true) {
            return;
        };

        if (isInRange(row, col) && mineField[row][col].value === 0) {
            setMatrix({ ...mineField }, mineField[row][col].isOpened = true,);
            // if ( mineField[row][col].isMarked === true ) {
            //     mineField[row][col].isMarked = false;
            //     setMinesLeft(minesLeft + 1);
            // };
        };

        for (let i = -1; i < 2; ++i) {
            for (let j = -1; j < 2; ++j) {
                if (i === 0 && j === 0) {
                    continue;
                };
                if (isInRange(row + i, col + j) && mineField[row + i][col + j].value > 0) {
                    setMatrix({ ...mineField }, mineField[row + i][col + j].isOpened = true);
                };
            };
        };

        if (isInRange(row - 1, col) && mineField[row - 1][col].value === 0) { // UP CHECK RECURSIVE
            revealEmptiness(row - 1, col);
        };

        if (isInRange(row, col + 1) && mineField[row][col + 1].value === 0) { // RIGHT CHECK RECURSIVE
            revealEmptiness(row, col + 1);
        };

        if (isInRange(row + 1, col) && mineField[row + 1][col].value === 0) { // DOWN CHECK RECURSIVE
            revealEmptiness(row + 1, col);
        };

        if (isInRange(row, col - 1) && mineField[row][col - 1].value === 0) { // LEFT CHECK RECURSIVE
            revealEmptiness(row, col - 1);
        };
    };

    const funfun = (x, y) => {
        
        if (mineField[x][y].isMarked === true) {
            mineField[x][y].isMarked = false;
            setMinesLeft(minesLeft + 1);
            console.log('funfun');
        }
        return <></>;
    };

    const outputField = () => {
        return mineField.map((row, rowIndex) => row.map((_, colIndex) =>
            <Mine
                key={[rowIndex, colIndex]}
                position={[rowIndex, colIndex]}
                isMine={mineField && mineField[rowIndex][colIndex].isMine}
                isOpened={mineField[rowIndex][colIndex].isOpened}
                value={mineField[rowIndex][colIndex].value}
                isFieldsVisible={areFieldsVisible}
                revealEmptiness={revealEmptiness}
                setMinesLeft={setMinesLeft}
                minesLeft={minesLeft}
                newGame={newGame}
                mineField={mineField}
                setMatrix={setMatrix}
                isMarked={mineField[rowIndex][colIndex].isMarked}
                generateField={generateField}
                funfun={funfun}
            />));
    };



    return (
        <div>
            <div className="mines-left">Mines left: {minesLeft}</div>
            <button onClick={() => startNewGame()}>New Game</button>
            <div className="minefield" style={{
                display: 'grid',
                gridTemplateColumns: `repeat(${cols}, ${rows}fr)`,
                width: '100px',
                marginLeft: '28%',
                position: 'absolute',
            }}>
                {/* {mineField.map((row, rowIndex) => row.map((_, colIndex) =>
                    <Mine
                        key={[rowIndex, colIndex]}
                        position={[rowIndex, colIndex]}
                        isMine={mineField && mineField[rowIndex][colIndex].isMine}
                        isOpened={mineField[rowIndex][colIndex].isOpened}
                        value={mineField[rowIndex][colIndex].value}
                        isFieldsVisible={areFieldsVisible}
                        showAll={showAll}
                        revealEmptiness={revealEmptiness}
                        setMinesLeft={setMinesLeft}
                        minesLeft={minesLeft}
                    />))} */}
                {outputField()}
            </div>
        </div>
    );
};

export default Minefield;