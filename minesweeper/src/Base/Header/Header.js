import React, { useContext } from 'react';
import './Header.css';
import { ModeContext } from '../../providers/ModeProvider';

const Header = () => {
    const { showAll } = useContext(ModeContext);
    return (
        <div className="header">
            { /* <div>

                <button className="mode-button"
                    onClick={setMode} >
                    {
                        developMode
                            ? 'Develop Mode'
                            : 'Play Mode'
                    }
                </button>
            </div>
            <button className="Elena-mode-button"
                onClick={setElenaMode}
            >Elena mode {ElenaMode ? "ON" : "OFF"}</button> */}
            <button onClick={showAll}>Reveal Mine field</button> 
        </div>
    );
};

export default Header;