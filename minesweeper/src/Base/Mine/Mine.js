import React, { useState, useContext, useEffect } from 'react';
import { ModeContext } from '../../providers/ModeProvider';
import './Mine.css';

const Mine = ({
    isMine,
    value,
    isOpened,
    setMinesLeft,
    minesLeft,
    revealEmptiness,
    position,
    newGame,
    isFieldsVisible,
    mineField,
    setMatrix,
    isMarked,
    generateField,
    funfun
}) => {

    const { areFieldsVisible, showAll } = useContext(ModeContext);
    const [isValueVisible, toggleValueVisible] = useState(areFieldsVisible);
    const [marker, setMarker] = useState(false);
    // generateField({...mineField}, mineField[position[0]][position[1]].isMarked = 1);


    useEffect(() => {
        toggleValueVisible(false);
        mineField[position[0]][position[1]].isMarked = false;
    }, [newGame]);

    const setFlag = (e) => {
        e.preventDefault();
        if (isValueVisible || isOpened) return;
        if (mineField[position[0]][position[1]].isMarked === true) {
            mineField[position[0]][position[1]].isMarked = false;
            setMarker(true);
        } else {
            mineField[position[0]][position[1]].isMarked = true;
            setMarker(false);
        }

        mineField[position[0]][position[1]].isMarked ? setMinesLeft(minesLeft - 1) : setMinesLeft(minesLeft + 1);
    };

    // const funfun = () => {
    //     console.log('funfun');
    //     if (mineField[position[0]][position[1]].isMarked === true) {
    //         mineField[position[0]][position[1]].isMarked = false;
    //         setMinesLeft(minesLeft + 1);
    //     }
    //     return <></>;
    // };


    return (
        <button className={`mine${value} ${isOpened && value < 1 && "openedEmptiness"}`}
            onClick={() => {
                if (!(mineField[position[0]][position[1]].isMarked) && !areFieldsVisible) {
                    toggleValueVisible(true);
                    if (value === 0) {
                        revealEmptiness(position[0], position[1]);
                    };
                    if (isMine === true) {
                        showAll();
                    };
                }
            }
            }
            onContextMenuCapture={e => {
                setFlag(e);
            }}
            // onContextMenuCapture={setFlag}
        >
            {((isValueVisible || isFieldsVisible || isOpened)
                ? (isMine
                    ? <svg className="mine-svg" height="20" width="20" xmlns="http://www.w3.org/2000/svg" viewBox="-35 0 900 800"><path d="M416 96a10.662 10.662 0 01-4.8-1.152c-22.933-11.541-87.829-41.515-112.533-41.515-18.821-3.175-37.205 7.618-43.605 25.6-2.631 5.271-9.037 7.411-14.308 4.779a10.666 10.666 0 01-5.148-13.483C245.25 44.43 271.334 28.618 298.667 32 332.8 32 411.904 71.339 420.8 75.819c5.255 2.663 7.356 9.082 4.693 14.336A10.666 10.666 0 01416 96z" fill="#ffc107" /><g fill="#ffd54f"><path d="M416 160c-5.891 0-10.667-4.776-10.667-10.667V128c0-5.891 4.776-10.667 10.667-10.667s10.667 4.776 10.667 10.667v21.333c0 5.891-4.776 10.667-10.667 10.667zM416 53.333c-5.891 0-10.667-4.776-10.667-10.667V21.333c0-5.891 4.776-10.667 10.667-10.667s10.667 4.776 10.667 10.667v21.333c0 5.892-4.776 10.667-10.667 10.667zM461.248 141.248a10.663 10.663 0 01-7.552-3.115l-15.083-15.083c-4.093-4.237-3.975-10.99.262-15.083a10.666 10.666 0 0114.821 0l15.083 15.083c4.171 4.16 4.179 10.914.019 15.085a10.667 10.667 0 01-7.571 3.134l.021-.021zM480 96h-21.333C452.776 96 448 91.224 448 85.333s4.776-10.667 10.667-10.667H480c5.891 0 10.667 4.776 10.667 10.667S485.891 96 480 96zM446.165 65.835a10.667 10.667 0 01-7.552-18.219l15.083-15.083c4.237-4.093 10.99-3.976 15.083.262a10.666 10.666 0 010 14.821l-15.083 15.083a10.67 10.67 0 01-7.531 3.136zM370.752 141.248a10.667 10.667 0 01-7.552-18.219l15.083-15.083c4.093-4.237 10.845-4.354 15.083-.262 4.237 4.093 4.354 10.845.262 15.083-.086.089-.173.176-.262.262l-15.083 15.083a10.666 10.666 0 01-7.531 3.136z" /></g><path d="M314.091 105.237A233.194 233.194 0 00181.333 64a10.665 10.665 0 00-10.133 7.296l-18.091 54.315a10.667 10.667 0 001.813 10.069 10.923 10.923 0 009.408 3.925 160.96 160.96 0 01115.989 33.515 10.667 10.667 0 006.613 2.283c4.311.003 8.2-2.59 9.856-6.571l21.12-50.731a10.665 10.665 0 00-3.817-12.864z" fill="#607d8b" /><path d="M181.333 117.333C81.186 117.333 0 198.519 0 298.667S81.186 480 181.333 480s181.333-81.186 181.333-181.333c-.117-100.099-81.234-181.216-181.333-181.334z" fill="#455a64" /><path d="M53.333 309.333c-5.891 0-10.667-4.776-10.667-10.667.095-76.544 62.123-138.572 138.667-138.666 5.891 0 10.667 4.776 10.667 10.667s-4.776 10.667-10.667 10.667C116.566 181.416 64.082 233.899 64 298.667c0 5.891-4.776 10.666-10.667 10.666z" fill="#607d8b" /></svg>
                    : (value === 0)
                        ? funfun(position[0], position[1])
                        : value
                )
                : '')}
            {
                (mineField[position[0]][position[1]].isMarked) && <div style={{ color: "red" }}><svg className="flag-svg" height="16" width="16" xmlns="http://www.w3.org/2000/svg" viewBox="7 34 500 500"><path d="M506.43 421.537L291.573 49.394c-15.814-27.391-55.327-27.401-71.147 0L5.568 421.537c-15.814 27.391 3.934 61.616 35.574 61.616h429.714c31.629 0 51.394-34.215 35.574-61.616zm-35.574 23.973H41.142a3.416 3.416 0 01-2.975-5.152L253.024 68.215a3.416 3.416 0 015.949 0L473.83 440.357a3.416 3.416 0 01-2.974 5.153z" /><path d="M255.999 184.991c-10.394 0-18.821 8.427-18.821 18.821v107.89c0 10.394 8.427 18.821 18.821 18.821s18.821-8.427 18.821-18.821v-107.89c.001-10.394-8.426-18.821-18.821-18.821zM255.999 354.975c-10.394 0-18.821 8.427-18.821 18.821v11.239c0 10.394 8.427 18.821 18.821 18.821s18.821-8.427 18.821-18.821v-11.239c.001-10.396-8.426-18.821-18.821-18.821z" /></svg>
                </div>
            }
        </button>
    );
};

export default Mine;

